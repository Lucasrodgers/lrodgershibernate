package lrodgershibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtils {


    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernate.cfg.xml").build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();

        } catch (Throwable e) {

            System.err.println("Initial Session Factory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }


    public static class DAO {

        SessionFactory factory;
        Session session = null;

        private static DAO single_instance = null;

        private DAO()
        {
            factory = HibernateUtils.getSessionFactory();
        }

        public static DAO getInstance()
        {
            if (single_instance == null) {
                single_instance = new DAO();
            }

            return single_instance;
        }

        //This function gets the books from the database
        public hpSeries gethpSeries(int id) {

            try {
                session = factory.openSession();
                session.getTransaction().begin();
                String sql = "from lrodgershibernate.hpSeries where id=" + id;

                hpSeries c = (hpSeries)session.createQuery(sql).getSingleResult();
                session.getTransaction().commit();
                return c;

            } catch (Exception e) {
                e.printStackTrace();

                // Rollback in case of an error occurred.
                session.getTransaction().rollback();
                return null;
            } finally {
                session.close();
            }
        }


        //This function updates the times read column in the database
        public void setReadCount(hpSeries t, int updateReadCount){


            hpSeries hp;


            session = factory.openSession();
            session.getTransaction().begin();

            hp=t;
            hp.setTimesRead(updateReadCount);

            session.update(hp);
            session.getTransaction().commit();
            session.close();
        }

    }




    public static void shutdown() {
        getSessionFactory().close();
    }

}

