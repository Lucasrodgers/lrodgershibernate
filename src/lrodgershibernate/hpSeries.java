package lrodgershibernate;

import javax.persistence.*;

@Entity
@Table(name = "harry_potter_books")
public class hpSeries {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "book_name")
    private String bookName;

    @Column(name = "page_count")
    private String pageCount;

    @Column(name = "times_read")
    private int timesRead;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return bookName;
    }

    public void setName(String name) {
        this.bookName = bookName;
    }

    public String getPageCount() {
        return pageCount;
    }

    public void setPageCount(String phone) {
        this.pageCount = pageCount;
    }

    public int getTimesRead() { return timesRead; }

    public void setTimesRead(int timesRead) { this.timesRead = timesRead; }

    public String toString() {
        return "\n\nHarry Potter book " + (id) + " is titled: " + bookName + ". It has " + pageCount +
                " pages. You have read it " + timesRead + " times." ;
    }
}
