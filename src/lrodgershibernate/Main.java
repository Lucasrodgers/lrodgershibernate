package lrodgershibernate;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        //HibernateUtils util = new HibernateUtils();
        //finds which book the user wants to look for
        System.out.println("Which book number would you like to find? \n");
        Scanner scanner = new Scanner(System.in);
        int bookID = scanner.nextInt();

        //puts out the name of the book with the page count and read count
        HibernateUtils.DAO t = HibernateUtils.DAO.getInstance();
        hpSeries hp;
        hp = t.gethpSeries(bookID);
        System.out.println(hp);

        //asks the user if they want to update the read count in the database
        System.out.println("Would you like to update your read count? (Y/N):");
        Scanner sc = new Scanner(System.in);

        char yesOrNo = sc.next().charAt(0);
        if(yesOrNo == 'Y' || yesOrNo == 'y'){
            System.out.println("How many times have you read this book?: ");
            Scanner numTimes = new Scanner(System.in);
            int updateReadCount = numTimes.nextInt();

            //updates the database with the new read count
           t.setReadCount(hp, updateReadCount);

            System.out.println("Database updated! \nHere is the updated info:");
            System.out.println(hp);

        }else if (yesOrNo == 'N' || yesOrNo == 'n') {
            System.out.println("Thank you for your time");
        }

    }
}
